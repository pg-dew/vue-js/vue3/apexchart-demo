import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Demo from "@/views/Demo";
import LineDemo1 from "@/components/line/LineDemo1";
import LineDemo2 from "@/components/line/LineDemo2";
import LineDemo3 from "@/components/line/LineDemo3";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/demo",
    component: Demo,
    children: [
      {
        path: "line/1",
        component: LineDemo1,
      },
      {
        path: "line/2",
        component: LineDemo2,
      },
      {
        path: "line/3",
        component: LineDemo3,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
